#This is an input file for Benchmark III problem of electrostatics coupled
#with Cahn-Hilliard.  Curved computational domain, no-flux boundary conditions.

[Mesh]
  type = GeneratedMesh
  dim = 2
  nx = 100
  ny = 100
  xmin = 0
  xmax = 100
  ymin = 0
  ymax = 100
[]

[Variables]
  [./c]
    order = FIRST
    family = LAGRANGE
    [./InitialCondition]
      type = BenchmarkIConcIC
      base_value = 0.5
      epsilon = 0.04
      q1 = '0.2 0.11 0'
      q2 = '0.13 0.087 0'
      q3 = '0.025 0.15 0'
      q4 = '0.07 0.02 0'
      constant = 0
  [../]
  [../]
  [./mu]
    order = FIRST
    family = LAGRANGE
  [../]

  [./phi]
    order = FIRST
    family = LAGRANGE
  [../]
[]

[AuxVariables]
  [./total_energy]
    order = CONSTANT
    family = MONOMIAL
  [../]
[]

[Preconditioning]
  [./SMP]
    type = SMP
    full = true
  [../]
[]

[Materials]
  [./generic]
    type = ElectrochemicalMaterial
#    CH_mobility = 1
    kappa_CH = 2

    c_alpha = 0.3
    c_beta = 0.7

    w = 5
    k = 0.3 #1 #0.3 #0.27 #9e-2
    epsilon = 100 #333 #100 #9e1

    concentration = c
    potential = phi
  [../]
  [./mobility]
    type = DerivativeParsedMaterial
    constant_names = 'M0'
    constant_expressions = '1'
    args = c
    f_name = M
    function = 'M0/((1+c)*(1+c))'
    derivative_order = 1
  [../]
[]


[Kernels]
  [./mu_residual]
    type = SplitCHWRes
    variable = mu
    mob_name = M
#    args = c
  [../]

  [./dcdt]
    type = CoupledTimeDerivative
    variable = mu
    v = c
  [../]

  [./c_residual]
    type = ElectrochemicalCH
    variable = c
    w = mu
    potential = phi
    kappa_name = kappa_CH
  [../]

  [./phi_residual]
    type = ElectrochemicalPoisson
    variable = phi
    c = c
    c0 = mass  # this grabs the postprocessor value for c in the system
    domain_volume = volume
  [../]
[]

[AuxKernels]
  [./total_free_energy_density]
    type = ElectrochemicalEnergy
    variable = total_energy
    CH_var = c
  [../]
[]


[BCs]
  [./fixed_phi]
    type = FunctionDirichletBC
    function = potential_bc
    variable = phi
    boundary = left
  [../]
    [./fixed_phi_grounded]
    type = DirichletBC
    value = 0.0
    variable = phi
    boundary = right
  [../]
[]

[Functions]
  [./potential_bc]
    type = ParsedFunction
    value = 2*sin(y/7)
  [../]
[]

[Postprocessors]
  [./numDOFs]
    type = NumDOFs
    system = NL
  [../]
  [./TotalEnergy]
    type = ElementIntegralVariablePostprocessor
    variable = total_energy
  [../]
  [./dt]
    type = TimestepSize
  [../]
  [./NL_iter]
    type = NumNonlinearIterations
  [../]
  [./mass]
    type = ElementIntegralVariablePostprocessor
    variable = c
    execute_on = timestep_begin
  [../]
  [./volume]
    type = VolumePostprocessor
    execute_on = initial
  [../]
[]

[Postprocessors]
  [./DOFs]
    type = NumDOFs
    system = NL
  [../]
[]

[Functions]
  [./inlet_func]
    type = ParsedFunction
    value = '-0.001 * (y - 3)^2 + 0.009'
  [../]
[]

[Executioner]
  type = Transient

  [./TimeStepper]
    type = IterationAdaptiveDT
    dt = 0.1
    cutback_factor = 0.95
    growth_factor = 1.05
    optimal_iterations = 5
    iteration_window = 1
    linear_iteration_ratio = 100
  [../]

  start_time = 0
  end_time = 401
  dtmin = 1e-6

  solve_type = 'NEWTON'
  petsc_options_iname = '-pc_type -sub_pc_type -sub_ksp_type -pc_asm_overlap'
  petsc_options_value = ' asm      lu           preonly       1'

  timestep_tolerance = 1e-5

  l_max_its = 100
  nl_abs_tol = 1e-11

[]

[Outputs]
  exodus = true
  interval = 10
  csv = true
  file_base = testElectrochemical_new
  checkpoint = true
  sync_times = '5 10 20 50 100 200 400'

  [./console]
    type = Console
    #perf_log = true
  [../]
[]
