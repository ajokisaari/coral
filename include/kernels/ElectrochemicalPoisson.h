/*************************************************************************
*
*  Welcome to Coral!
*  Andrea M. Jokisaari
*
*  29 June 2017
*
*************************************************************************/

#ifndef ELECTROCHEMICALPOISSON_H
#define ELECTROCHEMICALPOISSON_H

#include "Kernel.h"

class ElectrochemicalPoisson;

template<>
InputParameters validParams<ElectrochemicalPoisson>();

class ElectrochemicalPoisson : public Kernel
{
public:
  ElectrochemicalPoisson(const InputParameters & parameters);

protected:
  virtual Real computeQpResidual();

  virtual Real computeQpJacobian();

  virtual Real computeQpOffDiagJacobian(unsigned jvar);

  const VariableValue & _c;
  unsigned _c_var_number;

  const MaterialProperty<Real> & _epsilon;
  const MaterialProperty<Real> & _k;

  const PostprocessorValue & _c0;
  const PostprocessorValue & _vol;


private:

};

#endif //ELECTROCHEMICALPOISSON_H
