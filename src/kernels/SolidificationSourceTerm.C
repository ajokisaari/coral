/******************************************************
 *
 *   Welcome to Coral!
 *
 *   CHiMaD (ANL/Northwestern University)
 *
 *   Developer: Andrea Jokisaari
 *
 *   8 December 2016
 *
 *****************************************************/

#include "SolidificationSourceTerm.h"

template<>
InputParameters validParams<SolidificationSourceTerm>()
{
  InputParameters params =  validParams<CoupledTimeDerivative>();

  return params;
}

SolidificationSourceTerm::SolidificationSourceTerm(const InputParameters & parameters) :
    CoupledTimeDerivative(parameters)
{
}

Real
SolidificationSourceTerm::computeQpResidual()
{
  return -0.5*CoupledTimeDerivative::computeQpResidual();
}

Real
SolidificationSourceTerm::computeQpOffDiagJacobian(unsigned int jvar)
{
  if (jvar == _v_var)
    return -0.5*_test[_i][_qp] * _phi[_j][_qp] * _dv_dot[_qp];

  return 0.0;
}
