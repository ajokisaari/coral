/*************************************************************************
*
*  Welcome to Hackathon!
*  Andrea M. Jokisaari
*
*  1 October 2015
*
*************************************************************************/

#ifndef BENCHMARKICHCOUPLEDSPLIT_H
#define BENCHMARKICHCOUPLEDSPLIT_H

#include "SplitCHCRes.h"

class BenchmarkICHCoupledSplit;

template<>
InputParameters validParams<BenchmarkICHCoupledSplit>();

class BenchmarkICHCoupledSplit : public SplitCHCRes
{
public:
  BenchmarkICHCoupledSplit(const InputParameters & parameters);

protected:
  virtual Real computeDFDC(PFFunctionType type);
  virtual Real computeHeaviside();
  virtual Real computeDHeavisideDn(unsigned int i);
  virtual Real computeQpOffDiagJacobian(unsigned int jvar);

private:
  const MaterialProperty<Real> & _c_alpha;
  const MaterialProperty<Real> & _c_beta;

  unsigned int _n_OP_variables;
  std::vector<const VariableValue *> _OP;
  std::vector<unsigned int> _OP_vars;

  Real _factor;

};

#endif //BENCHMARKICHCOUPLEDSPLIT_H
