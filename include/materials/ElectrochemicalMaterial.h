/*************************************************************************
*
*  Welcome to Coral!
*  Andrea M. Jokisaari
*
*  27 June 2017
*
*************************************************************************/


#ifndef ELECTROCHEMICALMATERIAL_H
#define ELECTROCHEMICALMATERIAL_H

#include "Material.h"

//forward declarations
class ElectrochemicalMaterial;

template<>
InputParameters validParams<ElectrochemicalMaterial>();

class ElectrochemicalMaterial : public Material
{
public:
  ElectrochemicalMaterial(const InputParameters & parameters);

protected:
  virtual void computeQpProperties();

//  MaterialProperty<Real> & _M;
  MaterialProperty<Real> & _kappa_CH;

  MaterialProperty<Real> & _w;
  MaterialProperty<Real> & _c_alpha;
  MaterialProperty<Real> & _c_beta;

  MaterialProperty<Real> & _fbulk;
  MaterialProperty<Real> & _dfbulkdc;
  MaterialProperty<Real> & _d2fbulkdc2;

  MaterialProperty<Real> & _felec;

  MaterialProperty<Real> & _k;
  MaterialProperty<Real> & _epsilon;

  //these store the values from the input file.
  Real _M_param;
  Real _kappa_CH_param;

  Real _w_param;
  Real _c_alpha_param;
  Real _c_beta_param;

  Real _k_param;
  Real _epsilon_param;

  const VariableValue & _c;
  const VariableValue & _potential;

private:

};

#endif //ELECTROCHEMICALMATERIAL_H
