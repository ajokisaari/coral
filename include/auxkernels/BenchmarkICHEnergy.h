/*************************************************************************
*
*  Welcome to Hackathon!
*  Andrea M. Jokisaari
*
*  22 February 2016
*
*************************************************************************/
#ifndef BENCHMARKICHENERGY_H
#define BENCHMARKICHENERGY_H

#include "AuxKernel.h"

//Forward Declarations
class BenchmarkICHEnergy;

template<>
InputParameters validParams<BenchmarkICHEnergy>();

class BenchmarkICHEnergy : public AuxKernel
{
public:
  BenchmarkICHEnergy(const InputParameters & parameters);

protected:
  virtual Real computeValue();

private:
  const MaterialProperty<Real> & _c_alpha;
  const MaterialProperty<Real> & _c_beta;
  const MaterialProperty<Real> & _w;
  const MaterialProperty<Real> & _kappa_CH;

  const VariableValue & _c;
  const VariableGradient & _grad_c;

};

#endif //BENCHMARKICHENERGY_H
